﻿using System;
using Shared.Core.Providers;
using Tizen.Network.Bluetooth;
using Xamarin.Forms.Xaml;
using Tizen.Wearable.CircularUI.Forms;


namespace TizenWearableXamlApp1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : CirclePage
    {
        public MainPage()
        {
            InitializeComponent();
            SetRunningStatus();
        }

        public void Click(object sender, EventArgs eventArgs)
        {
            if (ServicesProvider.ScannerServiceIsRunning && ServicesProvider.AdvertiserServiceIsRunning)
            {
                ServicesProvider.StopScannerService();
                ServicesProvider.StopAdvertiserService();
            }
            else
            {
                ServicesProvider.StartScannerService();
                ServicesProvider.StartAdvertiserService();
            }

            SetRunningStatus();
        }

        private void SetRunningStatus()
        {
            if (ServicesProvider.ScannerServiceIsRunning && ServicesProvider.AdvertiserServiceIsRunning)
            {
                StatusLabel.Text = "Application is working..";
                StatusButton.Text = "Stop";
            }
            else
            {
                StatusLabel.Text = "Application is stopped...";
                StatusButton.Text = "Start";
            }
        }
    }
}