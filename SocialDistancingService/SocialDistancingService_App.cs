﻿using Tizen.Applications;
using TizenWearableXamlApp1.Tools;

namespace SocialDistancingService
{
    class App : ServiceApplication
    {
        protected override void OnCreate()
        {
            base.OnCreate();
            //AlarmManager.CreateAlarm(DateTime.Now, new AppControl(true){ ApplicationId = ""})
            // BleService.Scan();
        }

        protected override void OnAppControlReceived(AppControlReceivedEventArgs e)
        {
            base.OnAppControlReceived(e);

            BleService.Scan();

            ReceivedAppControl receivedAppControl = e.ReceivedAppControl;
            if (receivedAppControl.IsReplyRequest)
            {
                AppControl replyRequest = new AppControl();
                receivedAppControl.ReplyToLaunchRequest(replyRequest, AppControlReplyResult.Succeeded);
            }
        }

        protected override void OnDeviceOrientationChanged(DeviceOrientationEventArgs e)
        {
           // base.OnDeviceOrientationChanged(e);
        }

        protected override void OnLocaleChanged(LocaleChangedEventArgs e)
        {
          //  base.OnLocaleChanged(e);
        }

        protected override void OnLowBattery(LowBatteryEventArgs e)
        {
          //  base.OnLowBattery(e);
        }

        protected override void OnLowMemory(LowMemoryEventArgs e)
        {
          //  base.OnLowMemory(e);
        }

        protected override void OnRegionFormatChanged(RegionFormatChangedEventArgs e)
        {
           //base.OnRegionFormatChanged(e);
        }

        protected override void OnTerminate()
        {
           // base.OnTerminate();
        }

        static void Main(string[] args)
        {
            App app = new App();
            app.Run(args);
        }
    }
}
