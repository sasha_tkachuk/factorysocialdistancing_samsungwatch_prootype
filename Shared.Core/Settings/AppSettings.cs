﻿namespace Shared.Core.Settings
{
    public class AppSettings
    {
        public const string SyncServiceApiUrl = "https://tva-web-03.radiantrfid.com/Fsd/api/v1/Smartwatch";
        public const int MessageTimeoutSeconds = 10;
        public const int TagTimeoutSeconds = 20;
        public const int ClosestRssi = -81;
        public const int SamsungCompanyIdentifier = 117;
        public const string IdentityServiceUuid = "1012";
    }
}
