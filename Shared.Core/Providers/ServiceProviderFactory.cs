﻿using Shared.Core.Services;

namespace Shared.Core.Providers
{
    public class ServiceProviderFactory
    {
        private static readonly object Locker = new object();

        private static ServiceClientProvider provider;

        public static ServiceClientProvider Provider
        {
            get
            {
                if (provider == null)
                {
                    lock (Locker)
                    {
                        if (provider == null)
                            provider = new ServiceClientProvider();
                    }
                }

                return provider;
            }
        }

    }
}
