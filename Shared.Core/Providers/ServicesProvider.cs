﻿using System;
using Tizen.Applications;

namespace Shared.Core.Providers
{
    public static class ServicesProvider
    {
        private static string scannerServiceApplicationId = "org.tizen.example.ScannerService";
        private static string advertiserServiceApplicationId = "org.tizen.example.AdvertiserService";

        public static bool ScannerServiceIsRunning => ApplicationManager.IsRunning(scannerServiceApplicationId);

        public static bool AdvertiserServiceIsRunning => ApplicationManager.IsRunning(advertiserServiceApplicationId);

        public static void StartScannerService()
        {
            var appControl = new AppControl
            {
                ApplicationId = scannerServiceApplicationId,
                Operation = AppControlOperations.Default
            };

            AppControl.SendLaunchRequest(appControl, (request, replyRequest, result) =>
            {
                switch (result)
                {
                    case AppControlReplyResult.Succeeded:
                        Console.WriteLine("Service application is successfully launched.");
                        break;
                    case AppControlReplyResult.Failed:
                        Console.WriteLine("Service application is not launched.");
                        break;
                    case AppControlReplyResult.AppStarted:
                        Console.WriteLine("Service application is just started.");
                        break;
                    case AppControlReplyResult.Canceled:
                        Console.WriteLine("Service application is canceled.");
                        break;
                }
            });
        }

        public static void StartAdvertiserService()
        {
            var appControl = new AppControl
            {
                ApplicationId = advertiserServiceApplicationId,
                Operation = AppControlOperations.Default
            };

            AppControl.SendLaunchRequest(appControl, (request, replyRequest, result) =>
            {
                switch (result)
                {
                    case AppControlReplyResult.Succeeded:
                        Console.WriteLine("Service application is successfully launched.");
                        break;
                    case AppControlReplyResult.Failed:
                        Console.WriteLine("Service application is not launched.");
                        break;
                    case AppControlReplyResult.AppStarted:
                        Console.WriteLine("Service application is just started.");
                        break;
                    case AppControlReplyResult.Canceled:
                        Console.WriteLine("Service application is canceled.");
                        break;
                }
            });
        }

        public static void StopScannerService()
        {
            /*AppControl.SendTerminateRequest(new AppControl
            {
                ApplicationId = scannerServiceApplicationId,
                Operation = AppControlOperations.Default
            });*/
        }

        public static void StopAdvertiserService()
        {
            /*AppControl.SendTerminateRequest(new AppControl
            {
                ApplicationId = advertiserServiceApplicationId,
                Operation = AppControlOperations.Default
            });*/
        }
    }
}
