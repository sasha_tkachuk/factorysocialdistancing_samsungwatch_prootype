﻿using Tizen.Network.Bluetooth;

namespace Shared.Core.Providers
{
    public static class DeviceInfoProvider
    {
        private static string address;
        public static string Address => address ?? (address = BluetoothAdapter.Address);

        public static string DeviceUID
        {
            get
            {
              //  Information.TryGetValue("http://tizen.org/system/tizenid", out string id);
                //return id;
                return Address;
            }
        }
    }
}
