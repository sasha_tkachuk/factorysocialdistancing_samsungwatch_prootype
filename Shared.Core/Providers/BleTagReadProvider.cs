﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Shared.Core.Settings;
using Shared.Core.Utils;
using Tizen.Network.Bluetooth;
using TizenWearableXamlApp1.Tools;

namespace TizenWearableXamlApp1.Providers
{
    public class BleTagReadProvider
    {
        private readonly ConcurrentDictionary<string, BleTag> tags = new ConcurrentDictionary<string, BleTag>();
        private static readonly object Locker = new object();


        public BleTag[] Procees(BluetoothLeDevice device)
        {
            if (device != null)
            {
                if (TryGetWatchData(device, out var deviceId))
                {
                    var readTime = DateTime.Now;
                    var nearByMe = device.Rssi >= AppSettings.ClosestRssi;

                    if (tags.TryGetValue(deviceId, out var tag))
                    {
                        tag.ReadTime = readTime;
                        tag.Device = device;
                        tag.Notified = !nearByMe ? false : tag.Notified;
                        tag.NearByMe = nearByMe;
                    }
                    else
                    {
                        tags[deviceId] = new BleTag
                        {
                            ReadTime = readTime,
                            Device = device,
                            NearByMe = nearByMe
                        };
                    }

                    LogService.Debug($"device:{deviceId}: {device.Rssi}");
                }
            }

            RemoveOldReads();

            return tags.Where(t => t.Value.NearByMe).Select(t => t.Value).ToArray();
        }


        public bool TryGetWatchData(BluetoothLeDevice device, out string deviceId)
        {
            deviceId = null;

            if (device.ManufacturerData != null && device.ManufacturerData.DataLength == 6 && device.ManufacturerData.Id == AppSettings.SamsungCompanyIdentifier)
            {
                deviceId = StringUtils.ByteArrayToMac(device.ManufacturerData.Data);
                return true;
            }

            return false;
        }

        private void RemoveOldReads()
        {
            var time = DateTime.Now.AddSeconds(-AppSettings.TagTimeoutSeconds);
            string[] keys;
            lock (Locker)
                keys = tags.Where(d => d.Value.ReadTime < time).Select(x => x.Key).ToArray();

            foreach (var key in keys)
            {
                tags.TryRemove(key, out var x);
                LogService.Debug($"Removed obsolete: {key}");
            }
        }

        public class BleTag
        {
            public DateTime ReadTime { get; set; }
            public BluetoothLeDevice Device { get; set; }
            public bool NearByMe { get; set; }
            public bool Notified { get; set; }
        }
    }
}
