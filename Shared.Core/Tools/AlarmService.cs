﻿using System;
using System.Collections.Concurrent;
using Shared.Core.Settings;
using Tizen.Applications.Notifications;
using Tizen.Common;
#if WATCH_APP
using Tizen.Wearable.CircularUI.Forms;
#endif

namespace TizenWearableXamlApp1.Tools
{
    public static class AlarmService
    {
        private static ConcurrentDictionary<string, DateTime> messages = new ConcurrentDictionary<string, DateTime>();
        
        public static bool Alarm(string title, string message, Level level)
        {
            if (messages.TryGetValue(message, out var messageTime))
            {
                if (DateTime.Now.AddSeconds(-AppSettings.MessageTimeoutSeconds) < messageTime)
                    return false;
            }

            messages[message] = DateTime.Now;
            
            Notification.AccessorySet set = null;

            if (level == Level.High)
            {
                set = new Notification.AccessorySet
                {
                    CanVibrate = true,
                    LedColor = Color.Red,
                    LedOffMillisecond = 300,
                    LedOnMillisecond = 50,
                    LedOption = AccessoryOption.On
                };
            }
            else if (level == Level.Low)
            {
                set = new Notification.AccessorySet
                {
                    CanVibrate = true,
                    LedColor = Color.Purple,
                    LedOffMillisecond = 1000,
                    LedOnMillisecond = 100,
                    LedOption = AccessoryOption.On
                };
            }
            
            var notification = new Notification
            {
                Title = title,
                Content = message,
                IsVisible = true,
                Accessory = set,
                IsOngoing = false,
                Count = 20,
                Tag = "message",
                TimeStamp = DateTime.Now
            };

            NotificationManager.Post(notification);

            return true;
        }

        public static void Toast(string message)
        {
#if WATCH_APP     
            Tizen.Wearable.CircularUI.Forms.Toast.DisplayText("Bluetooth is not enabled");
#else
            Alarm("Warning", "Bluetooth is not enabled. Please enable bluetooth.", Level.Low);
#endif
        }

        public enum Level
        {
            Low,
            High
        }
    }
}
