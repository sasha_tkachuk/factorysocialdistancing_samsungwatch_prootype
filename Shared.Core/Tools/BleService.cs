﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Tizen.Network.Bluetooth;
using TizenWearableXamlApp1.Providers;
using Shared.Core.Settings;
using Shared.Core.Utils;

namespace TizenWearableXamlApp1.Tools
{
    public static class BleService
    {
        private static readonly BleTagReadProvider provider = new BleTagReadProvider();
        private static string address;
        private static BluetoothLeAdvertiseData leAdvertiseData;
        private static BluetoothLeAdvertiser advertiser;

        static BleService()
        {
            BluetoothAdapter.ScanResultChanged += BluetoothAdapter_ScanResultChanged;
            BluetoothAdapter.StateChanged += BluetoothAdapterOnStateChanged;

            advertiser = BluetoothAdapter.GetBluetoothLeAdvertiser();
            advertiser.AdvertisingStateChanged += AdvertiserOnAdvertisingStateChanged;
        }

        public static string Address => address ?? (address = BluetoothAdapter.Address);

        public static BluetoothLeAdvertiseData LeAdvertiseData
        {
            get
            {
                if (leAdvertiseData == null)
                {
                    leAdvertiseData = new BluetoothLeAdvertiseData
                    {
                        AdvertisingConnectable = false,
                        AdvertisingMode = BluetoothLeAdvertisingMode.BluetoothLeAdvertisingLowLatencyMode,
                        // IncludeDeviceName = false,
                        //IncludeTxPowerLevel = true,
                        PacketType = BluetoothLePacketType.BluetoothLeAdvertisingPacket,
                    };


                    // adv.IncludeDeviceName = false;
                    var array = StringUtils.MacToByteArray(Address);
                    BluetoothServiceData serviceData = new BluetoothServiceData();
                    serviceData.Uuid = AppSettings.IdentityServiceUuid;
                    serviceData.DataLength = 3;
                    serviceData.Data = new byte[3] { 0x01, 0x02, 0x03 };
                    leAdvertiseData.AddAdvertisingServiceData(BluetoothLePacketType.BluetoothLeAdvertisingPacket, serviceData);

                    leAdvertiseData.AddAdvertisingManufacturerData(
                        BluetoothLePacketType.BluetoothLeAdvertisingPacket,
                        new ManufacturerData
                        {
                            Data = array,
                            DataLength = array.Length,
                            Id = AppSettings.SamsungCompanyIdentifier
                        });
                }

                return leAdvertiseData;
            }
        }

        public static void Scan()
        {
            try
            {
                bool isEnabled = false;
                isEnabled = BluetoothAdapter.IsBluetoothEnabled;
                if (!isEnabled)
                {
                    AlarmService.Toast("Bluetooth is not enabled");
                    return;
                }

                BluetoothAdapter.StopLeScan();
                BluetoothAdapter.StartLeScan();
            }
            catch (Exception e)
            {
                LogService.Error(e);
                AlarmService.Toast("Unable to start scanning.");
            }
        }

        public static void Advertize()
        {
            Task.Factory.StartNew(AdvertizeLocal);
        }

        public static void AdvertizeLocal()
        {
            try
            {
                //advertiser.StopAdvertising(adv); 
                advertiser.StartAdvertising(LeAdvertiseData);

            }
            catch (Exception ex)
            {
                LogService.Error(ex);
            }
        }

        private static void AdvertiserOnAdvertisingStateChanged(object sender, AdvertisingStateChangedEventArgs e)
        {
            if (e.State == BluetoothLeAdvertisingState.BluetoothLeAdvertisingStopped)
            {
                Advertize();
            }
        }

        private static void BluetoothAdapterOnStateChanged(object sender, StateChangedEventArgs e)
        {
            try
            {
                if (e.BTState == BluetoothState.Enabled)
                {
                    Scan();
                }
            }
            catch (Exception exception)
            {
                LogService.Error(exception);
            }
        }

        private static void BluetoothAdapter_ScanResultChanged(object sender, AdapterLeScanResultChangedEventArgs e)
        {
            try
            {
                var closestItems = provider.Procees(e.DeviceData);
                if (closestItems.Length == 1)
                {
                    if (!closestItems[0].Notified)
                    {
                        if (AlarmService.Alarm(
                            "Warning",
                            "Other person in close proximity!",
                            AlarmService.Level.Low))
                        {
                            closestItems[0].Notified = true;
                        }
                    }
                }
                else if (closestItems.Length > 1)
                {
                    if (closestItems.Any(i => !i.Notified))
                    {
                        if (AlarmService.Alarm(
                            "Warning",
                            $"Cluster of more than {closestItems.Length} people detected!",
                            AlarmService.Level.High))
                        {
                            foreach (var closestItem in closestItems)
                            {
                                closestItem.Notified = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogService.Error(ex);
            }
        }
    }
}
