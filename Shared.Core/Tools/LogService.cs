﻿using System;
using Tizen;

namespace TizenWearableXamlApp1.Tools
{
    public static class LogService
    {
        private const string Tag = "RADIANT_WATCH_APP";
        public static void Debug(string message)
        {
            Log.Debug(Tag, message);
        }

        public static void Error(Exception exception)
        {
            Log.Error(Tag, exception.ToString());
        }

        public static void Error(string message)
        {
            Log.Error(Tag, message);
        }
    }
}
