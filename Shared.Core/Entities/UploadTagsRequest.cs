﻿using System;
using System.Collections.Generic;

namespace Shared.Core.Entities
{
    public class UploadTagsRequest
    {
        public string DeviceID { get; set; }

        public DateTime EventTime { get; set; }

        public List<BleTag> Tags { get; set; }
    }
}
