﻿using System;

namespace Shared.Core.Entities
{
    public class BleTag
    {
        public string DeviceID { get; set; }

        public int Rssi { get; set; }

        public DateTime SeenTime { get; set; }
    }
}
