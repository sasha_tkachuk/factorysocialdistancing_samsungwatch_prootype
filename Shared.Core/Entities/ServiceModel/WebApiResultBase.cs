﻿using System.Net;

namespace Shared.Core.Entities.ServiceModel
{
    public class WebApiResultBase
    {
        public HttpStatusCode StatusCode { get; set; }

        public bool IsSuccessStatusCode { get; set; }
    }
}
