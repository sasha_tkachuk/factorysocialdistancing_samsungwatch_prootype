﻿using Shared.Core.Services.Interfaces;

namespace Shared.Core.Services.Abstraction
{
    public interface IServiceClientProvider
    {
        ISyncService SyncService { get; }
    }
}
