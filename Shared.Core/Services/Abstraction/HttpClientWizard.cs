﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Shared.Core.Entities.ServiceModel;
using TizenWearableXamlApp1.Tools;

namespace Shared.Core.Services.Abstraction
{
    public class HttpClientWizard
    {
        private static readonly object Locker = new object();

        private HttpClient client;
        private readonly Dictionary<string, string> headers;
        private object body;
        private Uri serviceUri;

        public HttpClientWizard(string serviceUrl)
        {
            headers = new Dictionary<string, string>();
            serviceUri = new Uri(serviceUrl);
            AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        }

        public HttpClientWizard CreateRequest(string endPointName = null)
        {
            serviceUri = new Uri(serviceUri + "/" + endPointName);
            client = new HttpClient
            {
                MaxResponseContentBufferSize = 104857600, 
                Timeout = TimeSpan.FromMinutes(3)
            };

            return this;
        }

        public HttpClientWizard CreateRequest(Uri uri, string endPointName = null)
        {
            serviceUri = new Uri(uri + "/" + endPointName);
            client = new HttpClient
            {
                MaxResponseContentBufferSize = 104857600,
                Timeout = TimeSpan.FromMinutes(3)
            };

            return this;
        }

        public HttpClientWizard SetTimeout(TimeSpan timeout)
        {
            client.Timeout = timeout;
            return this;
        }

        public HttpClientWizard SetBaseAddress(Uri uri)
        {
            client.BaseAddress = uri;
            return this;
        }

        public HttpClientWizard AddBody(object bodyParam)
        {
            body = bodyParam;
            return this;
        }

        public HttpClientWizard AddHeader(string headerName, string headerValue)
        {
            if (!string.IsNullOrWhiteSpace(headerValue))
                headers[headerName] = headerValue;

            return this;
        }

        public virtual async Task<TResult> PostAsync<TResult>(MemoryStream stream, string fileName) where TResult : WebApiResultBase, new()
        {
            TResult result = null;
            try
            {
                ApplyHeaders();
                stream.Seek(0, SeekOrigin.Begin);
                HttpContent file = new StreamContent(stream);

                HttpContent content = new MultipartFormDataContent
                {
                    file
                };

                using (var response = await client.PostAsync(serviceUri, content).ConfigureAwait(false))
                {
                    try
                    {
                        response.EnsureSuccessStatusCode();
                        using (var responseContent = response.Content)
                        {
                            var resultContent = await responseContent.ReadAsStringAsync();
                            if (!string.IsNullOrEmpty(resultContent))
                            {
                                result = JsonConvert.DeserializeObject<TResult>(resultContent);
                            }
                        }

                        if (result == null)
                            result = new TResult();

                        result.StatusCode = response.StatusCode;
                        result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    }
                    catch (Exception exception)
                    {
                        LogService.Error(exception);
                        result = new TResult
                        {
                            IsSuccessStatusCode = false, 
                            StatusCode = response.StatusCode
                        };
                        throw;
                    }
                }
            }
            catch (Exception exception)
            {
                LogService.Error(exception);
            }

            return result;
        }

        public virtual async Task<TResult> PostAsync<TResult>() where TResult : WebApiResultBase, new()
        {
            TResult result = null;
            try
            {
                HttpContent content = null;

                ApplyHeaders();
                
                if (body != null)
                {
                    JsonSerializerSettings microsoftDateFormatSettings = new JsonSerializerSettings
                    {
                        DateFormatHandling = DateFormatHandling.IsoDateFormat,
                        DateTimeZoneHandling = DateTimeZoneHandling.Utc
                    };

                    var json = JsonConvert.SerializeObject(body, microsoftDateFormatSettings);
                    content = new StringContent(json, Encoding.UTF8, "application/json");
                }
                
                using (var response = await client.PostAsync(serviceUri, content).ConfigureAwait(false))
                {
                    try
                    {
                        response.EnsureSuccessStatusCode();
                        using (var responseContent = response.Content)
                        {
                            var resultContent = await responseContent.ReadAsStringAsync();
                            if (!string.IsNullOrEmpty(resultContent))
                            {
                                result = JsonConvert.DeserializeObject<TResult>(resultContent);
                            }
                        }

                        if (result == null)
                            result = new TResult();

                        result.StatusCode = response.StatusCode;
                        result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    }
                    catch (Exception exception)
                    {
                        LogService.Error(exception);
                        result = new TResult
                        {
                            IsSuccessStatusCode = false, 
                            StatusCode = response.StatusCode
                        };
                        throw;
                    }
                }
            }
            catch (Exception exception)
            {
                LogService.Error(exception);
            }

            return result;
        }

        public virtual async Task<TResult> GetAsync<TResult>() where TResult : WebApiResultBase, new()
        {
            TResult result = null;
            try
            {
                ApplyHeaders();
                using (var response = await client.GetAsync(serviceUri).ConfigureAwait(false))
                {
                    try
                    {
                        response.EnsureSuccessStatusCode();

                        using (var responseContent = response.Content)
                        {
                            var resultContent = await responseContent.ReadAsStringAsync();
                            if (!string.IsNullOrEmpty(resultContent))
                            {
                                result = JsonConvert.DeserializeObject<TResult>(resultContent);
                            }
                        }

                        if (result == null)
                            result = new TResult();

                        result.StatusCode = response.StatusCode;
                        result.IsSuccessStatusCode = response.IsSuccessStatusCode;
                    }
                    catch (Exception exception)
                    {
                        LogService.Error(exception);
                        result = new TResult
                        {
                            StatusCode = response.StatusCode, 
                            IsSuccessStatusCode = false
                        };
                        throw;
                    }
                }
            }
            catch (Exception exception)
            {
                LogService.Error(exception);
            }

            return result;
        }

        public virtual async Task<string> GetAsync()
        {
            try
            {
                ApplyHeaders();
                return await client.GetStringAsync(serviceUri).ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                LogService.Error(exception);
            }

            return null;
        }

        private void ApplyHeaders()
        {
            lock (Locker)
            {
                foreach (var header in headers)
                {
                    try
                    {
                        client.DefaultRequestHeaders.Remove(header.Key);
                    }
                    catch (Exception exception)
                    {
                        LogService.Error(exception);
                    }

                    client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                }
            }
        }
    }
}
