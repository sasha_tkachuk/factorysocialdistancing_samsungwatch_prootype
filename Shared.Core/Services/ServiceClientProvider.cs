﻿using Shared.Core.Services.Abstraction;
using Shared.Core.Services.Implementation;
using Shared.Core.Services.Interfaces;

namespace Shared.Core.Services
{
    public class ServiceClientProvider : IServiceClientProvider
    {
        public ServiceClientProvider()
        {
            SyncService = new SyncService();
        }

        public ISyncService SyncService { get; }
    }
}
