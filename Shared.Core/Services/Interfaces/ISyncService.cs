﻿using System.Threading.Tasks;
using Shared.Core.Entities;

namespace Shared.Core.Services.Interfaces
{
    public interface ISyncService : IService
    {
        Task<UploadTagsResult> UploadTags(UploadTagsRequest request);
    }
}
