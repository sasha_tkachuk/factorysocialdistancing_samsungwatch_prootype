﻿using System;
using System.Threading.Tasks;
using Shared.Core.Entities;
using Shared.Core.Services.Interfaces;
using Shared.Core.Settings;

namespace Shared.Core.Services.Implementation
{
    public class SyncService : ServiceBase, ISyncService
    {
        protected override string ServiceUrl => AppSettings.SyncServiceApiUrl;

        public async Task<UploadTagsResult> UploadTags(UploadTagsRequest request)
        {
            return await HttpClient.CreateRequest().
                SetTimeout(TimeSpan.FromMinutes(5)).
                AddBody(request).
                PostAsync<UploadTagsResult>();
        }
    }
}
