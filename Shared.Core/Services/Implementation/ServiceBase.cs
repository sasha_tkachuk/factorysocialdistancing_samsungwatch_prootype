﻿using Shared.Core.Services.Abstraction;

namespace Shared.Core.Services.Implementation
{
    public abstract class ServiceBase
    {
        protected abstract string ServiceUrl { get; }

        protected virtual HttpClientWizard HttpClient => new HttpClientWizard(ServiceUrl);
    }
}
