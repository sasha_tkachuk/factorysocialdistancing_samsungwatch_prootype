﻿using System;
using System.Linq;

namespace Shared.Core.Utils
{
    public static class StringUtils
    {
        public static byte[]  MacToByteArray(string mac)
        {
            return mac.Split(':').Select(x => Convert.ToByte(x, 16)).ToArray();
        }

        public static string ByteArrayToMac(byte[] array)
        {
            return BitConverter.ToString(array).Replace("-", ":");
        }
    }
}
